@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Login') }}</div>

                    <div class="card-body">
                        <form method="POST" action="{{ route('login') }}">
                            @csrf

                            <div class="row mb-3">
                                <label for="email"
                                    class="col-md-4 col-form-label text-md-end">{{ __('Email Address') }}</label>

                                <div class="col-md-6">
                                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror"
                                        name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                    @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="row mb-3">
                                <label for="password"
                                    class="col-md-4 col-form-label text-md-end">{{ __('Password') }}</label>

                                <div class="col-md-6">
                                    <input id="password" type="password"
                                        class="form-control @error('password') is-invalid @enderror" name="password"
                                        required autocomplete="current-password">

                                    @error('password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="row mb-3">
                                <div class="col-md-6 offset-md-4">
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" name="remember" id="remember"
                                            {{ old('remember') ? 'checked' : '' }}>

                                        <label class="form-check-label" for="remember">
                                            {{ __('Remember Me') }}
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="row mb-0">
                                <div class="col-md-8 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Login') }}
                                    </button>

                                    @if (Route::has('password.request'))
                                        <a class="btn btn-link" href="{{ route('password.request') }}">
                                            {{ __('Forgot Your Password?') }}
                                        </a>
                                    @endif
                                </div>
                            </div>
                        </form>
                        <button class="btn btn-primary mt-5" onclick="web3Login();">Log in with MetaMask</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="myModal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close btn" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">User Info</h4>
                </div>
                <div class="modal-body">
                    <form id="newModalForm">
                        @csrf
                        <div class="row">
                            <div class="form-group">
                                <label for="usr">Name:</label>
                                <input type="text" class="form-control" name="name" required>
                            </div>
                            <div class="form-group">
                                <label for="usr">Email:</label>
                                <input type="email" class="form-control" name="email" required>
                            </div>
                            <div class="form-group">
                                <label for="usr">Meta Address:</label>
                                <input type="text" class="form-control" name="metaAddress" value="" disabled>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-primary submitF">Save</button>
                        </div>
                    </form>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
@endsection

@section('script')
    <script src="https://cdn.jsdelivr.net/npm/web3@latest/dist/web3.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.14.0/jquery.validate.min.js"></script>
    <script>
        var web3;
        async function web3Login() {
            if (!window.ethereum) {
                alert('MetaMask not detected. Please install MetaMask first.');
                return;
            }
            await window.web3.currentProvider.enable().then().catch(error => console.log('error', error));
            web3 = new Web3(window.web3.currentProvider);
            var account = web3.eth.accounts.givenProvider.selectedAddress;
            // get chain id of metamask
            var chainId = await web3.eth.getChainId();
            var metaBalance = await web3.eth.getBalance(account).then(accountBal => {
                return accountBal
            }).catch(error => console.log('error', error));
            if (account) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: "POST",
                    url: "{{ route('addMetaMaskAddress') }}",
                    data: {
                        'account': account,
                        'chainId': chainId,
                        'metaBalance': metaBalance
                    },
                    success: function(data) {
                        if (data.status == 'success') {
                            $('input[name=metaAddress]').val(data.data.address);
                            $('input[name=metaAddress]').prop('disabled', true);
                            $('#myModal').modal({backdrop: 'static', keyboard: false});
                            $("#myModal").modal('show');
                        }
                    },
                    error: function(err) {
                        console.log('fail', err);
                    }
                });
            }
        }

        // pop up form validation
        $(function()
        {
            $("#newModalForm").validate({
                rules: {
                    name: {
                        required: true,
                    },
                    email: {
                        required: true,
                    }
                },
                messages: {
                    name: {
                        required: "Please enter your name!",
                    },
                    email: {
                        required: "Please enter your email address!",
                    },
                }
            });
            // pop up form request sent to server
            $('.submitF').click(function(e){
                // e.preventDefault();
                var uName = $('input[name=name]').val();
                var uEmail = $('input[name=email]').val();
                var uMetaAddress = $('input[name=metaAddress]').val();
                if (uName && uEmail)
                {
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        type: "POST",
                        url: "{{ route('addUserInfo') }}",
                        data: {
                            'uName': uName,
                            'uEmail': uEmail,
                            'uMetaAddress': uMetaAddress
                        },
                        success: function(data) {
                            //
                        },
                        error: function(err) {
                            console.log('fail', err);
                        }
                    });
                }
            })
        });
    </script>
@endsection
