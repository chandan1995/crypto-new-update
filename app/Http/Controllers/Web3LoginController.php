<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use kornrunner\Keccak;
use App\Models\User;
use Illuminate\Support\Facades\DB;

class Web3LoginController extends Controller
{
    public function addMetaMaskAddress(Request $request)
    {
        DB::beginTransaction();
        try{
            $account = $request->account;
            $chainId = $request->chainId;
            $metaBalance = $request->metaBalance;
            $checkAddress = DB::table('users')->where('address',$account)->first();
            if(empty($checkAddress))
            {
                $user = new User();
                $user->name = Str::random(40);
                $user->email = Str::random(10).'@gmail.com';
                $user->password = Hash::make(Str::random(8));
                $user->address = $account;
                $user->chainId = $chainId;
                $user->metaBalance = $metaBalance;
                $user->save();
                DB::commit();
                return ['status' => 'success', 'message' => 'data added successfully!', 'data' => $user];
            }
        } catch(\Exception $e) { 
            /* An error occured; cancel the transaction */
            DB::rollback();
            return ['status' => 'error', 'message' => $e->getMessage()];
        }
    }

    // user update information in users table
    public function addUserInfo(Request $request)
    {
        DB::beginTransaction();
        try{
            $uName = $request->uName;
            $uEmail = $request->uEmail;
            $uMetaAddress = $request->uMetaAddress;
            $Address = DB::table('users')->where('address',$uMetaAddress)->first();
            if(!empty($Address))
            {
                $user = User::find($Address->id);
                $user->name = $uName;
                $user->email = $uEmail;
                $user->password = Hash::make(Str::random(8));
                $user->address = $Address->address;
                $user->chainId = $Address->chainId;
                $user->metaBalance = $Address->metaBalance;
                $user->save();
               /* Commit the transaction */
                DB::commit();
                return ['status' => 'success', 'message' => 'data updated successfully!', 'data' => $user];
            }
        } catch(\Exception $e) { 
            /* An error occured; cancel the transaction */
            DB::rollback();
            return ['status' => 'error', 'message' => $e->getMessage()];
        }
    }
}
